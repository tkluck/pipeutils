r"""
A module that provides bash-like syntax for piping
input and output of subprocesses, without actually
exposing ourselves to the security risks and quoting
hazards of the shell=True option

EXAMPLES:

>>> (call('echo', 'a\nb') | call('head', '-n1')).output()
'a\n'
>>> (call('echo', 'a\nb') | call('head', '-n1')).do()
0
>>> (call('echo', 'Hello world!') > 'test.txt').do()
0
>>> (call('cat') < 'test.txt').output()
'Hello world!\n'
>>> call('false').check()
Traceback (most recent call last):
    ...
CalledProcessError: Command '<unavailable>' returned non-zero exit status 1
"""
import subprocess
PIPE = subprocess.PIPE

class Executable(object):
    def __init__(self):
        pass

    def do(self):
        return_code, output = self.do_full()
        return return_code

    def output(self):
        return_code, output = self.do_full()
        if return_code != 0:
            raise subprocess.CalledProcessError(return_code, '<unavailable>')
        return output

    def check(self):
        return_code, output = self.do_full()
        if return_code != 0:
            raise subprocess.CalledProcessError(return_code, '<unavailable>')

    def do_full(self):
        stdin, stdout = self._spawn(None, PIPE)
        return self._do_full()

    def __or__(self, other):
        class Pipe(Executable):
            def __init__(self, left, right):
                Executable.__init__(self)
                self._left = left
                self._right = right

            def _do_full(self):
                return self._right._do_full()

            def _spawn(self, stdin, stdout):
                stdin, pipe = self._left._spawn(stdin, PIPE)
                _, stdout = self._right._spawn(pipe, stdout)

                return stdin, stdout

        return Pipe(self, other)        

    def __gt__(self, file):
        class OutputRedirect(Executable):
            def __init__(self, left, right):
                Executable.__init__(self)
                self._left = left
                self._right = right

            def _do_full(self):
                return self._left._do_full()

            def _spawn(self, stdin, stdout):
                if isinstance(self._right,str):
                    out = open(self._right, 'w')
                else:
                    out = self._right
                stdin, stdout = self._left._spawn(stdin, out)
                return stdin, stdout

        return OutputRedirect(self, file)

    def __lt__(self, file):
        class InputRedirect(Executable):
            def __init__(self, left, right):
                Executable.__init__(self)
                self._left = left
                self._right = right

            def _do_full(self):
                return self._left._do_full()

            def _spawn(self, stdin, stdout):
                if isinstance(self._right,str):
                    i = open(self._right, 'r')
                else:
                    i = self._right
                stdin, stdout = self._left._spawn(i, stdout)
                return stdin, stdout

        return InputRedirect(self, file)


class Call(Executable):
    def __init__(self, *args):
        Executable.__init__(self)
        self.commands = args

    def _do_full(self):
        output, err = self._popen.communicate()
        return self._popen.returncode, output

    def _spawn(self, stdin, stdout):
        self._popen = subprocess.Popen(self.commands, stdin=stdin, stdout=stdout)
        return self._popen.stdin, self._popen.stdout


call = Call

if __name__ == "__main__":
    import doctest
    doctest.testmod()
